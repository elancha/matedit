#include <assert.h>

#define NOB_IMPLEMENTATION
#include "nob.h"

typedef enum {
	TARGET_LINUX,
	// TODO: TARGET_WIN64_MINGW right now means cross compilation from Linux to Windows using mingw-w64
	// I think the naming should be more explicit about that
	TARGET_WIN64_MINGW,
	TARGET_WIN64_MSVC,
	TARGET_MACOS,
	COUNT_TARGETS
} Target;

static_assert(4 == COUNT_TARGETS, "Amount of targets have changed");
const char *target_names[] = {
	[TARGET_LINUX]       = "linux",
	[TARGET_WIN64_MINGW] = "win64-mingw",
	[TARGET_WIN64_MSVC]  = "win64-msvc",
	[TARGET_MACOS]       = "macos",
};

void
log_available_targets(Nob_Log_Level level)
{
	nob_log(level, "Available targets:");
	for (size_t i = 0; i < COUNT_TARGETS; ++i) {
		nob_log(level, "    %s", target_names[i]);
	}
}

typedef struct {
	Target target;
} Config;

bool
compute_default_config(Config *config)
{
	memset(config, 0, sizeof(Config));
#ifdef _WIN32
#if defined(_MSC_VER)
	config->target = TARGET_WIN64_MSVC;
#else
	config->target = TARGET_WIN64_MINGW;
#endif
#else
#if defined (__APPLE__) || defined (__MACH__)
	config->target = TARGET_MACOS;
#else
	config->target = TARGET_LINUX;
#endif
#endif
	return true;
}

bool
parse_config_from_args(int argc, char **argv, Config *config)
{
	while (argc > 0) {
		const char *flag = nob_shift_args(&argc, &argv);
		if (strcmp(flag, "-t") == 0 || strcmp(flag, "--target") == 0) {
			if (argc <= 0) {
				nob_log(NOB_ERROR, "No value is provided for flag %s", flag);
				log_available_targets(NOB_ERROR);
				return false;
			}

			const char *value = nob_shift_args(&argc, &argv);

			bool found = false;
			for (size_t i = 0; !found && i < COUNT_TARGETS; ++i) {
				if (strcmp(target_names[i], value) == 0) {
					config->target = i;
					found = true;
				}
			}

			if (!found) {
				nob_log(NOB_ERROR, "Unknown target %s", value);
				log_available_targets(NOB_ERROR);
				return false;
			}
		} else if (strcmp("-h", flag) == 0 || strcmp("--help", flag) == 0) {
			nob_log(NOB_INFO, "Available config flags:");
			nob_log(NOB_INFO, "    -t <target>    set build target");
			nob_log(NOB_INFO, "    -h             print this help");
			return false;
		} else {
			nob_log(NOB_ERROR, "Unknown flag %s", flag);
			return false;
		}
	}
	return true;
}

void
log_config(Config config)
{
	nob_log(NOB_INFO, "Target: %s", NOB_ARRAY_GET(target_names, config.target));
}

bool
dump_config_to_file(const char *path, Config config)
{
	Nob_String_Builder sb = {0};
	nob_log(NOB_INFO, "Saving configuration to %s", path);
	nob_sb_append_cstr(&sb, nob_temp_sprintf("target = %s"NOB_LINE_END, NOB_ARRAY_GET(target_names, config.target)));
	bool res = nob_write_entire_file(path, sb.items, sb.count);
	nob_sb_free(sb);
	return res;
}

bool
load_config_from_file(const char *path, Config *config)
{
	bool result = true;
	Nob_String_Builder sb = {0};

	nob_log(NOB_INFO, "Loading configuration from %s", path);

	if (!nob_read_entire_file(path, &sb)) nob_return_defer(false);

	Nob_String_View content = {
		.data = sb.items,
		.count = sb.count,
	};

	for (size_t row = 0; content.count > 0; ++row) {
		Nob_String_View line = nob_sv_trim(nob_sv_chop_by_delim(&content, '\n'));
		if (line.count == 0) continue;
		if (line.data[0] == '#') continue;

		Nob_String_View key = nob_sv_trim(nob_sv_chop_by_delim(&line, '='));
		Nob_String_View value = nob_sv_trim(line);

		if (nob_sv_eq(key, nob_sv_from_cstr("target"))) {
			bool found = false;
			for (size_t t = 0; !found && t < COUNT_TARGETS; ++t) {
				if (nob_sv_eq(value, nob_sv_from_cstr(target_names[t]))) {
					config->target = t;
					found = true;
				}
			}
			if (!found) {
				nob_log(NOB_ERROR, "%s:%zu: Invalid target `"SV_Fmt"`", path, row + 1, SV_Arg(value));
				log_available_targets(NOB_ERROR);
				nob_return_defer(false);
			}
		} else {
			nob_log(NOB_ERROR, "%s:%zu: Invalid key `"SV_Fmt"`", path, row + 1, SV_Arg(key));
			nob_return_defer(false);
		}
	}

defer:
	nob_sb_free(sb);
	return result;
}

bool
build_matedit(Config config)
{
	bool result = true;
	Nob_Cmd cmd = {0};
	Nob_Procs procs = {0};

	switch (config.target) {
		case TARGET_LINUX: {
			cmd.count = 0;
			nob_cmd_append(&cmd, "cc");
			nob_cmd_append(&cmd, "-std=c99", "-Wall", "-Wextra", "-pedantic", "-ggdb");
			nob_cmd_append(&cmd, "-I./raylib-4.5.0/src/");
			nob_cmd_append(&cmd, "-o", "./build/matedit");
			nob_cmd_append(&cmd, "matedit.c");
			nob_cmd_append(&cmd,
				nob_temp_sprintf("-L./build/raylib/%s", NOB_ARRAY_GET(target_names, config.target)),
				"-l:libraylib.a");
			nob_cmd_append(&cmd, "-lm");
			if (!nob_cmd_run_sync(cmd)) nob_return_defer(false);
		} break;

		case TARGET_MACOS:
		case TARGET_WIN64_MINGW:
		case TARGET_WIN64_MSVC:
			nob_log(NOB_ERROR, "TODO: compiling is not supported on %s yet", NOB_ARRAY_GET(target_names, config.target));
			nob_return_defer(false);
			break;

        default: NOB_ASSERT(0 && "unreachable");
    }

defer:
	nob_cmd_free(cmd);
	nob_da_free(procs);
	return result;
}

static const char *raylib_modules[] = {
	"rcore",
	"raudio",
	"rglfw",
	"rmodels",
	"rshapes",
	"rtext",
	"rtextures",
	"utils",
};

bool
build_raylib(Config config)
{
	bool result = true;
	Nob_Cmd cmd = {0};
	Nob_File_Paths object_files = {0};

	if (!nob_mkdir_if_not_exists("./build/raylib")) nob_return_defer(false);

	Nob_Procs procs = {0};

	const char *build_path = nob_temp_sprintf("./build/raylib/%s", NOB_ARRAY_GET(target_names, config.target));

	if (!nob_mkdir_if_not_exists(build_path)) nob_return_defer(false);

	for (size_t i = 0; i < NOB_ARRAY_LEN(raylib_modules); ++i) {
		const char *input_path = nob_temp_sprintf("./raylib-4.5.0/src/%s.c", raylib_modules[i]);
		const char *output_path = nob_temp_sprintf("%s/%s.o", build_path, raylib_modules[i]);
		switch (config.target) {
			case TARGET_LINUX:
			case TARGET_MACOS:
			case TARGET_WIN64_MINGW:
				output_path = nob_temp_sprintf("%s/%s.o", build_path, raylib_modules[i]);
				break;
			case TARGET_WIN64_MSVC:
				output_path = nob_temp_sprintf("%s/%s.obj", build_path, raylib_modules[i]);
				break;
			default: NOB_ASSERT(0 && "unreachable");
		}

		nob_da_append(&object_files, output_path);

		if (nob_needs_rebuild(output_path, &input_path, 1)) {
			cmd.count = 0;
			switch (config.target) {
				case TARGET_LINUX:
					nob_cmd_append(&cmd, "cc");
					nob_cmd_append(&cmd, "-ggdb", "-DPLATFORM_DESKTOP", "-fPIC");
					nob_cmd_append(&cmd, "-I./raylib-4.5.0/src/external/glfw/include");
					nob_cmd_append(&cmd, "-c", input_path);
					nob_cmd_append(&cmd, "-o", output_path);
					break;
				case TARGET_MACOS:
					nob_cmd_append(&cmd, "clang");
					nob_cmd_append(&cmd, "-g", "-DPLATFORM_DESKTOP", "-fPIC");
					nob_cmd_append(&cmd, "-I./raylib-4.5.0/src/external/glfw/include");
					nob_cmd_append(&cmd, "-Iexternal/glfw/deps/ming");
					nob_cmd_append(&cmd, "-DGRAPHICS_API_OPENGL_33");
					if(strcmp(raylib_modules[i], "rglfw") == 0) {
						nob_cmd_append(&cmd, "-x", "objective-c");
					}
					nob_cmd_append(&cmd, "-c", input_path);
					nob_cmd_append(&cmd, "-o", output_path);
					break;
				case TARGET_WIN64_MINGW:
					nob_cmd_append(&cmd, "x86_64-w64-mingw32-gcc");
					nob_cmd_append(&cmd, "-ggdb", "-DPLATFORM_DESKTOP", "-fPIC");
					nob_cmd_append(&cmd, "-DPLATFORM_DESKTOP");
					nob_cmd_append(&cmd, "-fPIC");
					nob_cmd_append(&cmd, "-I./raylib-4.5.0/src/external/glfw/include");
					nob_cmd_append(&cmd, "-c", input_path);
					nob_cmd_append(&cmd, "-o", output_path);
					break;
				case TARGET_WIN64_MSVC:
					nob_cmd_append(&cmd, "cl.exe", "/DPLATFORM_DESKTOP");
					nob_cmd_append(&cmd, "/I", "./raylib-4.5.0/src/external/glfw/include");
					nob_cmd_append(&cmd, "/c", input_path);
					nob_cmd_append(&cmd, nob_temp_sprintf("/Fo%s", output_path));
					break;
				default: NOB_ASSERT(0 && "unreachable");
			}

			Nob_Proc proc = nob_cmd_run_async(cmd);
			nob_da_append(&procs, proc);
		}
	}
	cmd.count = 0;

	if (!nob_procs_wait(procs)) nob_return_defer(false);

	switch (config.target) {
		case TARGET_MACOS:
		case TARGET_LINUX:
		case TARGET_WIN64_MINGW: {
			const char *libraylib_path = nob_temp_sprintf("%s/libraylib.a", build_path);

			if (nob_needs_rebuild(libraylib_path, object_files.items, object_files.count)) {
				nob_cmd_append(&cmd, "ar", "-crs", libraylib_path);
				for (size_t i = 0; i < NOB_ARRAY_LEN(raylib_modules); ++i) {
					const char *input_path = nob_temp_sprintf("%s/%s.o", build_path, raylib_modules[i]);
					nob_cmd_append(&cmd, input_path);
				}
				if (!nob_cmd_run_sync(cmd)) nob_return_defer(false);
			}
		} break;
		case TARGET_WIN64_MSVC: {
			const char *libraylib_path = nob_temp_sprintf("%s/raylib.lib", build_path);
			if (nob_needs_rebuild(libraylib_path, object_files.items, object_files.count)) {
				nob_cmd_append(&cmd, "lib");
				for (size_t i = 0; i < NOB_ARRAY_LEN(raylib_modules); ++i) {
					const char *input_path = nob_temp_sprintf("%s/%s.obj", build_path, raylib_modules[i]);
					nob_cmd_append(&cmd, input_path);
				}
				nob_cmd_append(&cmd, nob_temp_sprintf("/OUT:%s", libraylib_path));
				if (!nob_cmd_run_sync(cmd)) nob_return_defer(false);
			}
		} break;
		default: NOB_ASSERT(0 && "unreachable");
	}

defer:
	nob_cmd_free(cmd);
	nob_da_free(object_files);
	return result;
}

bool
build_deploy(Config config)
{
	switch (config.target) {
		case TARGET_LINUX: {
			if (!nob_mkdir_if_not_exists("./matedit-linux-x86_64/")) return false;
			if (!nob_copy_file("./build/matedit", "./matedit-linux-x86_64/matedit")) return false;
			/* TODO: should we pack ffmpeg with Linux build?
			   There are some static executables for Linux */
			Nob_Cmd cmd = {0};
			nob_cmd_append(&cmd, "tar", "fvc", "./matedit-linux-x86_64.tar.gz", "./matedit-linux-x86_64");
			bool ok = nob_cmd_run_sync(cmd);
			nob_cmd_free(cmd);
			if (!ok) return false;
		} break;
		case TARGET_WIN64_MINGW: {
			if (!nob_mkdir_if_not_exists("./matedit-win64-mingw/")) return false;
			if (!nob_copy_file("./build/matedit.exe", "./matedit-win64-mingw/matedit.exe")) return false;
			/* TODO: pack ffmpeg.exe with windows build */
			Nob_Cmd cmd = {0};
			nob_cmd_append(&cmd, "zip", "-r", "./matedit-win64-mingw.zip", "./matedit-win64-mingw/");
			bool ok = nob_cmd_run_sync(cmd);
			nob_cmd_free(cmd);
			if (!ok) return false;
		} break;
		case TARGET_WIN64_MSVC: {
			nob_log(NOB_ERROR, "TODO: Creating distro for MSVC build is not implemented yet");
			return false;
		} break;
		case TARGET_MACOS: {
			nob_log(NOB_ERROR, "TODO: Creating distro for MacOS build is not implemented yet");
			return false;
		}
		default: NOB_ASSERT(0 && "unreachable");
	}

	return true;
}

void
log_available_subcommands(const char *program, Nob_Log_Level level)
{
	nob_log(level, "Usage: %s [subcommand]", program);
	nob_log(level, "Subcommands:");
	nob_log(level, "\tbuild (default)");
	nob_log(level, "\tconfig");
	nob_log(level, "\tdist");
	nob_log(level, "\thelp");
}

int
main(int argc, char **argv)
{
	NOB_GO_REBUILD_URSELF(argc, argv);

	const char *program = nob_shift_args(&argc, &argv);

	const char *subcommand = NULL;
	if (argc <= 0) {
		subcommand = "build";
	} else {
		subcommand = nob_shift_args(&argc, &argv);
	}

	if (strcmp(subcommand, "build") == 0) {
		Config config = {0};
		switch (nob_file_exists("./build/build.conf")) {
			case -1: /* ERROR */
				return 1;
			case 0: /* FILE DOES NOT EXIST */
				if (!nob_mkdir_if_not_exists("build")) return 1;
				if (!compute_default_config(&config)) return 1;
				if (!dump_config_to_file("./build/build.conf", config)) return 1;
				break;
			case 1: /* FILE EXISTS */
				if (!load_config_from_file("./build/build.conf", &config)) return 1;
				break;
		}
		nob_log(NOB_INFO, "------------------------------");
		log_config(config);
		nob_log(NOB_INFO, "------------------------------");
		if (!build_raylib(config)) return 1;
		if (!build_matedit(config)) return 1;
	} else if (strcmp(subcommand, "config") == 0) {
		Config config = {0};
		if (!nob_mkdir_if_not_exists("build")) return 1;
		if (!compute_default_config(&config)) return 1;
		if (!parse_config_from_args(argc, argv, &config)) return 1;
		nob_log(NOB_INFO, "------------------------------");
		log_config(config);
		nob_log(NOB_INFO, "------------------------------");
		if (!dump_config_to_file("./build/build.conf", config)) return 1;
	} else if (strcmp(subcommand, "dist") == 0) {
		Config config = {0};
		if (!load_config_from_file("./build/build.conf", &config)) return 1;
		nob_log(NOB_INFO, "------------------------------");
		log_config(config);
		nob_log(NOB_INFO, "------------------------------");
		if (!build_deploy(config)) return 1;
	} else if (strcmp(subcommand, "help") == 0){
		log_available_subcommands(program, NOB_INFO);
	} else {
		nob_log(NOB_ERROR, "Unknown subcommand %s", subcommand);
		log_available_subcommands(program, NOB_ERROR);
	}

	/* TODO: it would be nice to check for situations like building TARGET_WIN64_MSVC on Linux and report that it's not possible. */
	return 0;
}

