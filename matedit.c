#include <inttypes.h>
#include <math.h>
#include <stdio.h>

#include "raylib.h"
#include "raymath.h"

#define WIDTH 800
#define HEIGHT 450
#define MAX_SCENES 10

typedef uint64_t u64;
typedef int64_t s64;
typedef double f64;

Vector3
interpolate_position(f64 old_ft, Vector3 old_position, f64 now_ft, Vector3 target)
{
	f64 t_old = (now_ft - 1)/(old_ft - 1);
	return Vector3Add(Vector3Scale(old_position, t_old), Vector3Scale(target, now_ft));
}

f64
easeInOutExpo(float t)
{
	return t < 0.5 ? pow(2, 20*t - 10)/2 : (2 - pow(2, 10 - 20*t))/2;
}

u64 scene_number;
f64 elapsed_from_input;

struct {
	Vector3 position;
} moon;

struct {
	Vector3 position;
} sun;

void
initialize(void)
{
	moon.position = (Vector3){0, 0, 0};
	sun.position = (Vector3){0, 0, 0};
}

int
main(void)
{
	InitWindow(WIDTH, HEIGHT, "Test Presentation");
	SetTargetFPS(60);

	initialize();

	/* Define the camera to look into our 3d world */
	Camera camera = { 0 };
	camera.position = (Vector3){ 0.0f, 10.0f, 10.0f }; /* Camera position */
	camera.target = (Vector3){ 0.0f, 0.0f, 0.0f };      /* Camera looking at point */
	camera.up = (Vector3){ 0.0f, 1.0f, 0.0f };          /* Camera up vector (rotation towards target) */
	camera.fovy = 45.0f;                                /* Camera field-of-view Y */
	camera.projection = CAMERA_PERSPECTIVE;             /* Camera projection type */

	while (!WindowShouldClose()) {
		f64 dt = GetFrameTime();

		if (IsKeyPressed(KEY_RIGHT)) {
			if (scene_number < MAX_SCENES-1) {
				scene_number++;
				elapsed_from_input = 0;
			}
		}
		if (IsKeyPressed(KEY_LEFT)) {
			if (scene_number > 0) {
				scene_number--;
				elapsed_from_input = 0;
			}
		}
		
		BeginDrawing();
			ClearBackground(RAYWHITE);

			if (scene_number == 0) {
				BeginMode3D(camera);
				/* FIRST SCENE */
				if (elapsed_from_input < 0.5) {
					f64 old_ft = easeInOutExpo(2*elapsed_from_input);
					f64 new_ft = easeInOutExpo(2*(dt + elapsed_from_input));
					moon.position = interpolate_position(old_ft, moon.position, new_ft, (Vector3){2, 1, 2});
				}
				DrawSphereEx(moon.position, 4, 20, 20, BLUE);
				EndMode3D();
			} else if (scene_number == 1) {
				BeginMode3D(camera);
				moon.position = (Vector3){0, 0, 0};
				DrawSphereEx(moon.position, 4, 20, 20, BLUE);
				sun.position = (Vector3){0, 3, 0};
				DrawSphereEx(sun.position, 2, 20, 20, GOLD);
				EndMode3D();
			}

			DrawText(TextFormat("Scene: %d (%f)", scene_number, elapsed_from_input), 190, 200, 20, LIGHTGRAY);

		EndDrawing();

		elapsed_from_input += dt;
	}

	CloseWindow();
	return 0;
}
